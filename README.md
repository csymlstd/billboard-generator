<p align="center"><img width=60% src="https://gitlab.com/joaopluis/billboard-generator/raw/master/src/images/logo-color.png"></p>
<p align="center">
    <a href="#"><img src="https://img.shields.io/badge/license-GPL--3.0-blue.svg?style=flat"
         alt="License GPL-3.0"></a>
  <a href="https://www.paypal.me/joaopluis">
    <img src="https://img.shields.io/badge/€-donate-orange.svg?style=flat">
  </a>
</p>

The **Billboard Generator** is a set of tools designed to easily create billboards for
[Planet Coaster](http://planetcoaster.com). Currently it can generate two kinds of billboards, for rides and stores.

The project is live at http://pcbillboard.surge.sh.

It has no affiliation with the game or its developer, Frontier.

## Getting started

These instructions will get you a copy of the project up and running on your local machine for development purposes.
Remember, if you only want to use the generator, just use the link provided above.

### Prerequisites

You need [Yarn](https://yarnpkg.com) installed to build this project.

### Installation

First, use Yarn to install the project dependencies:

```
yarn install
```

Then, copy the `.env.example` file to `.env`. This file contains the "sentitive" settings - the ones that must not be
on Git. In this case, it only hosts the Google Analytics ID - which you may leave empty.

```
cp .env.example .env
```

Now you are ready to build the project! To run it for development, you can run

```
yarn run hot
```

which will open a dev server at http://localhost:8080.

## Deployment

If you want to build this project for production, you can run

```
yarn run production
```

which will build the project and store the generated files in the `dist` directory.

If you want to put your generator online, I strongly recommend [Surge](http://surge.sh). If you use Surge, the following
command builds the project and deploys it to Surge:

```
yarn run deploy
```

## Built with

* [Bulma](http://bulma.io/) - The CSS framework used
* [Vue.js](http://vuejs.org) - The JS framework used
* A slightly modified version of [html2canvas](https://html2canvas.hertzen.com/) - Used to convert the billboard `<div>` to an image
* [Font Awesome 5](https://fontawesome.com/) - The interface icons
* [The Noun Project](https://thenounproject.com/) - Some icons and images
* [Webpack](https://webpack.js.org/) - To build the project

## Versioning

This project has different version numbers:

* One for the main project
* One for each generator

Neither of those follow SemVer strictly.

## Author

* **João Luís** - http://joaopluis.pt

## License

This project is licensed under the GPL-3.0 license - see the [LICENSE](LICENSE) file for details.

## Acknowledgments

* Everyone at [r/planetcoaster](http://reddit.com/r/planetcoaster) for their suggestions and support.
* Everyone who contributed with translations - the list is in the About page.